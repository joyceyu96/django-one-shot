from django.shortcuts import render, redirect

from todos.models import TodoItem, TodoList
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from django.urls import reverse_lazy


from todos.forms import TodoListForm

# Create your views here.


def todo_list_list(request):
    # gets all instances of the TodoList model
    todo_list = TodoList.objects.all()

    # stores them in the variable context
    context = {"todo_list": todo_list}

    return render(request, "todos/list.html", context)


def show_todo_list_detail(request, pk):

    todo_list_instance = TodoList.objects.get(pk=pk)

    context = {"todo_list_instance": todo_list_instance}

    return render(request, "todos/detail.html", context)


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todo_items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todo_items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


# def create_todo_list(request):
#     if request.method == "POST":
#         form = TodoListForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect("todo_list_detail", pk=todolist.pk)

#     context = {"form": form}

#     return render(request, "todos/new.html", context)
