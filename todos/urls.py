from django.urls import path

from todos.views import (
    TodoItemCreateView,
    TodoListDeleteView,
    TodoListUpdateView,
    todo_list_list,
    show_todo_list_detail,
    TodoListCreateView,
    TodoItemUpdateView,
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:pk>/", show_todo_list_detail, name="todo_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),
    path(
        "<int:pk>/edit/", TodoListUpdateView.as_view(), name="todo_list_update"
    ),
    path(
        "<int:pk>/delete/",
        TodoListDeleteView.as_view(),
        name="todo_list_delete",
    ),
    path(
        "items/create/", TodoItemCreateView.as_view(), name="todo_item_create"
    ),
    path(
        "items/<int:pk>/edit/",
        TodoItemUpdateView.as_view(),
        name="todo_item_update",
    ),
]
